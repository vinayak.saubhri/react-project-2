import { useLocation } from "react-router-dom";
import Card from "../../Components/Card";
import "./style.css";

function Show() {
  const location = useLocation();
  return (
    <div className="show_container">
      <Card Blog={location.state.Blog.Blog} />
    </div>
  );
}
export default Show;
