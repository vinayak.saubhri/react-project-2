import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import "./style.css";
function Edit() {
  const navigate = useNavigate();
  const { id } = useParams();
  const [editTitle, setTitle] = useState("");
  const [editAuthorname, setAuthor] = useState("");
  const [editDescription, setDesc] = useState("");

  const handleUpdate = async () => {
    await fetch(`http://localhost:4000/blogs/${id}`, {
      method: "PATCH",
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
      body: JSON.stringify({
        authorName: editAuthorname,
        description: editDescription,
        title: editTitle,
      }),
    });
    navigate("/");
  };
  return (
    <div className="edit_container">
      <div className="text">
        <label htmlFor="title">Title:</label>
        <input
          name="title"
          type="text"
          value={editTitle}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="text">
        <label htmlFor="author">Author:</label>

        <input
          name="author"
          type="text"
          value={editAuthorname}
          onChange={(e) => setAuthor(e.target.value)}
        />
      </div>
      <div className="text">
        <label htmlFor="desc"> Description:</label>
        <input
          name="desc"
          type="text"
          value={editDescription}
          onChange={(e) => setDesc(e.target.value)}
        />
      </div>
      <div className="btn">
        <button onClick={handleUpdate}>Update</button>
      </div>
    </div>
  );
}
export default Edit;
