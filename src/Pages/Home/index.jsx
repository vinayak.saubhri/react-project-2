import { useEffect, useState } from "react";
import Card from "../../Components/Card";
import "./style.css";

function Home() {
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(false);
  const URL = "http://localhost:4000/blogs";

  useEffect(() => {
    const fetchBook = async () => {
      setLoading(true);
      const Data = await (await fetch(URL)).json();
      setBlogs(Data);
      setLoading(false);
    };
    fetchBook();
  }, []);

  return (
    <div className="blog_container">
      {blogs.map((blog) => (
        <Card
          Blog={blog}
          Loading={loading}
          key={blog.id}
          BlogArray={blogs}
          setBlogs={setBlogs}
        />
      ))}
    </div>
  );
}
export default Home;
