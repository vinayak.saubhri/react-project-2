import { Link, useNavigate } from "react-router-dom";

function Card({ Blog, Loading, BlogArray, setBlogs }) {
  let navigate = useNavigate();

  const deleteBlog = async (id) => {
    await fetch(`http://localhost:4000/blogs/${id}`, { method: "DELETE" });
    setBlogs(BlogArray.filter((Blogs) => Blogs.id !== id));
  };

  if (Loading) {
    return <h2>Loading....</h2>;
  }
  return (
    <div className="card" style={{ width: "18rem" }}>
      <img src={Blog.blog_image} className="card-img-top" alt="..." />
      <div className="card-body">
        <h4 className="card-title">{Blog.authorName}</h4>
        <h5>{Blog.title}</h5>
        <h6>{Blog.description}</h6>
        <div className="button_container">
          <Link to={`/blog/edit/${Blog.id}`}>
            <button className="btn btn-primary">Edit</button>
          </Link>
          <button
            onClick={() => {
              navigate(`/blog/${Blog.id}`, { state: { Blog: { Blog } } });
            }}
            className="btn btn-primary"
          >
            Show
          </button>
          <button
            className="btn btn-primary"
            onClick={() => deleteBlog(Blog.id)}
          >
            Delete
          </button>
        </div>
      </div>
    </div>
  );
}

export default Card;
