import { Link } from "react-router-dom";
import "./style.css";
function Navbar() {
  return (
    <div className="navbar_container">
      <Link to={"/"}>Home</Link>
      <Link to={"/about"}>About</Link>
      <Link to={"/contact"}>Contact</Link>
    </div>
  );
}
export default Navbar;
